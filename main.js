var myApp = angular.module('myApp', []);

myApp.factory('citiesService', function () {
    return {
        citiesData: {
            "Belarus" : ["Minsk", "Vitebsk", "Mogilev", "Grodno", "Gomel", "Brest"],
            "Russia" : ["Moskow", "S.Petersburg", "Smolensk", "Ufa", "Chita"],
            "Ukrain" : ["Kiev", "Lviv", "Odessa"]
        },
        selectedCountry: "Belarus"
    }
});


myApp.directive('countries',['citiesService', function (citiesService) {
        return {
            restrict: 'E',
            template: `<select type="text" ng-model="selectedCountry" ng-change="countryChanged(selectedCountry)" ng-options="country for country in countries"></select>`,
            scope: {},
            link: function (scope) {
                scope.countryChanged = function (selected) {
                    citiesService.selectedCountry = selected;
                };
                scope.countries = Object.keys(citiesService.citiesData);
                scope.selectedCountry = citiesService.selectedCountry;
            }
        }
    }]
);

myApp.directive('cities',[ 'citiesService', function (citiesService) {
        return {
            restrict: 'E',
            template: `<select type="text" ng-model="selectedCity" ng-options="city for city in cities"></select>`,
            scope: {},
            link: function (scope) {

                scope.$watch(function () {
                    return citiesService.selectedCountry;
                }, function (country) {
                    scope.cities = citiesService.citiesData[country];
                    scope.selectedCity = scope.cities[0];
                });

                scope.cities = citiesService.citiesData[citiesService.selectedCountry];
                scope.selectedCity = scope.cities[0];

            }
        }
    }]
);
